<?php

/**
 * @file
 * Contains Drupal\bee_15\Form\UserBookingConfigForm.
 */

namespace Drupal\bee_15\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Drupal\bee_15\Form\UserBookingConfigForm.
 */
class UserBookingConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bee_15.userbookings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bee_15_booking_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bee_15.userbookings');

    $form['cancelation_hours'] = [
      '#type' => 'textfield',
      '#size' => 5,
      '#maxlength' => 5,
      '#title' => $this->t('Booking cancelation hours before schedule time.'),
      '#description' => $this->t('For booking that are less than configured hours before the schedule time, users will not be able to cancel then.<br/>Leave blank to deny users cancel their bookings.'),
      '#default_value' => $config->get('cancelation_hours'),
      '#field_suffix' => $this->t('hours.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('cancelation_hours')) && !is_numeric($form_state->getValue('cancelation_hours'))) {
      $form_state->setErrorByName('cancelation_hours', $this->t('The value must be a number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('bee_15.userbookings')
      ->set('cancelation_hours', $form_state->getValue('cancelation_hours'))
      ->save();
  }

}
