<?php

namespace Drupal\bee_15\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\bat_booking\Entity\Booking;
use Drupal\bat_event\Entity\Event;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class ConfirmBookingCancelForm extends ConfirmFormBase {

  /**
   * ID of the booking to cancel.
   *
   * @var int
   */
  protected $id;

  /**
   * ID of the booking to cancel.
   *
   * @var int
   */
  protected $order_id;

  /**
   * Booking entity.
   *
   * @var \Drupal\bat_booking\Entity\Booking
   */
  protected $booking;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL, string $order_id = NULL) {
    $this->id = $id;
    $this->order_id = $order_id;
    $this->booking = Booking::load($this->id);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "confirm_booking_cancel_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $user_id = \Drupal::routeMatch()->getParameter('user_id');
    return Url::fromRoute('view.user_bookings.page_1', ['user' => $user_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to cancel your booking #%booking_id and order #%order_id with %label ?', [
      '%booking_id' => $this->booking->id(),
      '%order_id' => $this->order_id,
      '%label' => $this->booking->label()
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '<h3>' . $this->getQuestion() . '</h3>' . $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $event_id = $this->booking->get('booking_event_reference')->target_id;
    $event = Event::load($event_id);
    $event->set('event_state_reference', 4); // 'AV'
    $event->save();

    $this->messenger()->addMessage($this->t('Booking #%booking_id and order #%order_id with %label has been cancelled.', [
      '%booking_id' => $this->booking->id(),
      '%order_id' => $this->order_id,
      '%label' => $this->booking->label(),
    ]));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
